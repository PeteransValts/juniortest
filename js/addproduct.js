$(document).ready(function() {



    $('#addType1').hide();
    $('#property1').hide();
    $('#addType2').hide();
    $('#property2').hide();
    $('#addType3').hide();
    $('#property3').hide();
    $('#property1Error').hide();
    $('#property2Error').hide();
    $('#property3Error').hide();

    $(document).on('change', '#type', function() {
        var type = $('#type').val();
        if (type == "Book") {
            $('#addType1').show();
            $('#property1').show();
            $('#property1Error').show();
            $('#addType2').hide();
            $('#property2').hide();
            $('#property2Error').hide();
            $('#addType3').hide();
            $('#property3').hide();
            $('#property3Error').hide();

            $('#addType1').text("Weight");

            $('#help').text("Please specify weight in KG (Kilograms)");
        } else if (type == "CD") {
            $('#addType1').show();
            $('#property1').show();
            $('#property1Error').show();
            $('#addType2').hide();
            $('#property2').hide();
            $('#property2Error').hide();
            $('#addType3').hide();
            $('#property3').hide();
            $('#property3Error').hide();

            $('#addType1').text("Size");

            $('#help').text("Please specify size in MB (Megabytes)");
        } else if (type == "Furniture") {
            $('#addType1').text("Height");
            $('#addType2').text("Width");
            $('#addType3').text("Length");
            $('#addType1').show();
            $('#property1').show();
            $('#property1Error').show();
            $('#addType2').show();
            $('#property2').show();
            $('#property2Error').show();
            $('#addType3').show();
            $('#property3').show();
            $('#property3Error').show();
            $('#help').text("Please provide dimensions in CM (Centimeters)");
        } else if (type == "Choose Product") {
            $('#addType1').hide();
            $('#property1').hide();
            $('#property1Error').hide();
            $('#addType2').hide();
            $('#property2').hide();
            $('#property2Error').hide();
            $('#addType3').hide();
            $('#property3').hide();
            $('#property3Error').hide();
            $('#help').text("");
        }

    });


});
