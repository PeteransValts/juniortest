$(document).ready(function() {
    $('#addB').click(function() {
        var sku = $('#sku').val();
        var name = $('#name').val();
        var price = $('#price').val();
        var property1 = $('#property1').val();
        var property2 = $('#property2').val();
        var property3 = $('#property3').val();

        if (sku == "") {
            $('#skuError').text("This field can't be empty!");
        }
        else{
          if(sku.length > 10){
            $('#skuError').text("Maximum is 10 symbols!");
          }
        }
        if (name == "") {
            $('#nameError').text("This field can't be empty!");
        }
        if (price == "") {
            $('#priceError').text("This field can't be empty!");
        }
        if (property1 == "") {
            $('#property1Error').text("This field can't be empty!");
        }
        if (property2 == "") {
            $('#property2Error').text("This field can't be empty!");
        }
        if (property3 == "") {
            $('#property3Error').text("This field can't be empty!");
        }
    });
});
